const axios = require('axios');

class AudD {
    constructor(apiToken) {
        this.apiToken = apiToken;
        this.api = axios.create({
            baseURL: 'https://api.audd.io/',
        });
    };

    soundRequest(url) {
        return this.api.post('/', {
            url: url,
            api_token: this.apiToken,
            return: 'apple_music,spotify,deezer'
        })
    }

    lyricsRequest(lyrics) {
        return this.api.post('/', {
            method: 'findLyrics',
            q: lyrics,
            api_token: this.apiToken
        })
    }
}

module.exports = {
    audD: new AudD(process.env.AUDD_API_TOKEN)
};