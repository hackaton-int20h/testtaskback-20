const fs = require('fs');
const Joi = require('@hapi/joi');
const {v4} = require('uuid');
const BodyParser = require('../utils/body-parser');
const Validator = require('../utils/validator');
const {s3} = require('../providers/s3');
const {audD} = require('../providers/audd');

const validationSchema = Joi.object({
    sound: Joi.any().required()
});

const handler = async (event) => {
    try {
        const body = await BodyParser.parse(event);
        const {files} = body;
        const values = await Validator.validate({
            sound: files && files.sound
        }, validationSchema);
        const buffer = await fs.promises.readFile(values.sound.path);
        const fileExtension = values.sound.name.split('.').pop();
        const uid = v4();
        await s3.putObject({
            Bucket: process.env.BUCKET,
            Key: `${uid}.${fileExtension}`,
            Body: buffer
        }).promise();
        const url = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.BUCKET,
            Key: `${uid}.${fileExtension}`
        });
        const {data} = await audD.soundRequest(url.replace(' ',''));
        await s3.deleteObject({
            Bucket: process.env.BUCKET,
            Key: `${uid}.${fileExtension}`,
        });
        if (data.status === 'error') {
            return {
                statusCode: 400,
                body: JSON.stringify(data.error)
            }
        }
        let prepared = null;
        if (data.result) {
            prepared = {
                title: data.result.title,
                artist: data.result.artist
            };
            if (data.result.apple_music) {
                prepared['apple_music'] = data.result.apple_music.url
            }
            if (data.result.deezer) {
                prepared['deezer'] = data.result.deezer.link
            }
            if (data.result.spotify) {
                prepared['spotify'] = data.result.spotify.external_urls.spotify
            }
        }
        return {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            statusCode: 200,
            body: JSON.stringify(prepared)
        }
    } catch (err) {
        return {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            statusCode: 400,
            body: JSON.stringify(err.message || err)
        };
    }
};

module.exports = {
    handler
};
