const Joi = require('@hapi/joi');
const BodyParser = require('../utils/body-parser');
const Validator = require('../utils/validator');
const {audD} = require('../providers/audd');

const validationSchema = Joi.object({
    lyrics: Joi.string().required()
});

const handler = async (event) => {
    try {
        const body = await BodyParser.parse(event);
        const {lyrics} = await Validator.validate(body, validationSchema);
        const {data} = await audD.lyricsRequest(lyrics);
        if (data.status === 'error') {
            return {
                statusCode: 400,
                body: JSON.stringify(data.error)
            }
        }
        const parsed = data.result.map(song => {
            const media = JSON.parse(song.media);
            const providers = media.reduce((result, next) => {
                result[next.provider] = next.url;
                return result;
            }, {});
            return {
                title: song.title_with_featured,
                artist: song.artist,
                ...providers
            }
        });
        return {
            headers: {
                'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
            },
            statusCode: 200,
            body: JSON.stringify(parsed)
        }
    } catch (err) {
        return {
            headers: {
                'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
            },
            statusCode: 400,
            body: JSON.stringify(err.message || err)
        };
    }
};

module.exports = {
    handler
};