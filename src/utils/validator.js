const validate = async (obj, schema) => {
    return new Promise((resolve, reject) => {
        const {value, error} = schema.validate(obj, {
            allowUnknown: true,
        });
        if (error) {
            const errors = (error.details || []).reduce((general, next) => {
                return {
                    ...general,
                    [next.path.join('.')]: next.message
                };
            }, {});
            return reject(errors)
        }
        resolve(value)
    });
};

module.exports = {
    validate
};