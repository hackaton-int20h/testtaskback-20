const Busboy = require('busboy');
const os = require('os');
const {v4} = require('uuid');
const path = require('path');
const fs = require('fs');

function getContentType(event) {
    return event.headers['content-type']
        ? event.headers['content-type']
        : event.headers['Content-Type']
}

function parse(event) {
    return new Promise((resolve, reject) => {
        const result = {};
        const busboy = new Busboy({
            headers: {
                'content-type': getContentType(event),
            }
        });
        busboy.on('file', (fieldName, file, fileName, enc, mimeType) => {
            const fileExtension = fileName.split('.').pop();
            const name = v4();
            const saveTo = path.join(os.tmpdir(), `${name}.${fileExtension}`);
            file.pipe(fs.createWriteStream(saveTo));
            if (!result.files) {
                result.files = {};
            }
            result.files[fieldName] = {
                name: fileName,
                encoding: enc,
                mimeType: mimeType,
                truncated: file.truncated,
                path: saveTo
            };
        });
        busboy.on('field', (fieldName, value) => {
            result[fieldName] = value;
        });
        busboy.on('error', (err) => {
            reject(err);
        });
        busboy.on('finish', () => {
            resolve(result);
        });
        busboy.write(event.body, event.isBase64Encoded ? 'base64' : 'binary');
    });
}

module.exports = {
    parse
};